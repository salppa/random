export class chip8 {
  memory: string[] = new Array(4096).fill("00"); // 4096 bytes, stored as hex
  registers = new Array(16).fill(0); // registers 0...F (8-bit)
  stack: number[] = []; // stack pointer
  display: boolean[][] = []; //64x32px
  pc: number = 512; //program counter
  delayTimer = 0; // 8-bit number
  soundTimer = 0; // 8-bit number
  i = 0; // 16-bit register (usually used as memory pointer)
  clock = 0;
  keyboard = new Array(16).fill(false);
  soundEnabled = true;

  constructor(rom: ArrayBuffer) {
    const program = this.readProgram(rom);
    const memoryZeroPoint = 512; // memory below this address is reserved for chip8 interpreter
    program.map((x, i) => (this.memory[memoryZeroPoint + i] = x));
    this.initDisplay();
    this.initFonts();
    this.initKeyboard();
  }

  private initKeyboard() {
    /*
CHIP-8 keyboard looks like this:

1	2	3	C
4	5	6	D
7	8	9	E
A	0	B	F

This is mapped to:

1 2 3 4 
Q W E R
A S D F
Z X C V
    */
    const keyboardMap = {} as any;
    keyboardMap[49] = 0; // 1 => 1
    keyboardMap[50] = 1; // 2 => 2
    keyboardMap[51] = 2; // 3 => 3
    keyboardMap[52] = 3; // 4 => C
    keyboardMap[81] = 4; // Q => 4
    keyboardMap[87] = 5; // W => 5
    keyboardMap[69] = 6; // E => 6
    keyboardMap[82] = 7; // R => D
    keyboardMap[65] = 8; // A => 7
    keyboardMap[83] = 9; // S => 8
    keyboardMap[68] = 10; // D => 9
    keyboardMap[70] = 11; // F => E
    keyboardMap[90] = 12; // Z => A
    keyboardMap[88] = 13; // X => 0
    keyboardMap[67] = 14; // C => B
    keyboardMap[86] = 15; // V => F

    document.addEventListener("keydown", (e) => {
      this.keyboard[keyboardMap[e.keyCode]] = true;
    });
    document.addEventListener("keyup", (e) => {
      this.keyboard[keyboardMap[e.keyCode]] = false;
    });
  }

  private initFonts() {
    const initFont = (sprites: number[], memPosition: number) => {
      sprites.map((sprite, i) => {
        this.memory[memPosition * 5 + i] = this.decimalToHex(sprite);
      });
    };

    // see http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#2.4
    initFont([0xf0, 0x90, 0x90, 0x90, 0xf0], 0); // 0
    initFont([0x20, 0x60, 0x20, 0x20, 0x70], 1); // 1
    initFont([0xf0, 0x10, 0xf0, 0x80, 0xf0], 2); // 2
    initFont([0xf0, 0x10, 0xf0, 0x10, 0xf0], 3); // 3
    initFont([0x90, 0x90, 0xf0, 0x10, 0x10], 4); // 4
    initFont([0xf0, 0x80, 0xf0, 0x10, 0xf0], 5); // 5
    initFont([0xf0, 0x80, 0xf0, 0x90, 0xf0], 6); // 6
    initFont([0xf0, 0x10, 0x20, 0x40, 0x40], 7); // 7
    initFont([0xf0, 0x90, 0xf0, 0x90, 0xf0], 8); // 8
    initFont([0xf0, 0x90, 0xf0, 0x10, 0xf0], 9); // 9
    initFont([0xf0, 0x90, 0xf0, 0x90, 0x90], 10); // a
    initFont([0xe0, 0x90, 0xe0, 0x90, 0xe0], 11); // b
    initFont([0xf0, 0x80, 0x80, 0x80, 0xf0], 12); // c
    initFont([0xe0, 0x90, 0x90, 0x90, 0xe0], 13); // d
    initFont([0xf0, 0x80, 0xf0, 0x80, 0xf0], 14); // e
    initFont([0xf0, 0x80, 0xf0, 0x80, 0x80], 15); // f
  }

  private initDisplay() {
    for (let y = 0; y < 32; y++) {
      this.display[y] = [];
      for (let x = 0; x < 64; x++) {
        this.display[y][x] = false;
      }
    }
  }

  public time = -1;

  private beepAudio = new Audio(
    "data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU="
  );
  private beep = () => {
    this.beepAudio.currentTime = 0;
    this.beepAudio.play();
  };

  public stepOnce() {
    const oldPc = this.pc;
    const opCode = this.memory[this.pc] + this.memory[this.pc + 1];
    const type = this.execute(opCode);

    // timers are counter down at 60Hz - Clock speed is 500Hz
    if (this.clock % 8 === 0) {
      this.delayTimer = Math.max(this.delayTimer - 1, 0);

      if (this.soundTimer > 0) {
        this.soundTimer--;
        if (this.soundTimer === 0) {
          if (window.localStorage.getItem("soundEnabled") === "true") {
            this.beep();
          }
        }
      }
    }
    this.clock++;
    return `Executed ${oldPc.toString().padStart(2, "0")} ${opCode} ${type}`;
  }

  public step(fast = false) {
    let opCodes = [];

    const singleOpCodeTime = 2; // 500Hz => 1000/500=2ms per opcode
    let opCodesToRun =
      this.time === -1
        ? 1
        : Math.floor((window.performance.now() - this.time) / singleOpCodeTime);

    if (opCodesToRun > 1) {
      console.warn(
        "Skipping screen update for " + (opCodesToRun - 1) + " opcodes"
      );
    }
    for (let i = 0; i < opCodesToRun; i++) {
      opCodes.push(this.stepOnce());
    }

    this.time = window.performance.now();

    return fast ? [] : opCodes;
  }

  private readProgram(rom: ArrayBuffer) {
    const byteToHex: string[] = [];

    for (let n = 0; n <= 0xff; ++n) {
      const hexOctet = n.toString(16).padStart(2, "0");
      byteToHex.push(hexOctet);
    }

    function hex(arrayBuffer: ArrayBuffer) {
      const buff = new Uint8Array(arrayBuffer);
      const hexOctets = []; // new Array(buff.length) is even faster (preallocates necessary array size), then use hexOctets[i] instead of .push()

      for (let i = 0; i < buff.length; ++i) hexOctets.push(byteToHex[buff[i]]);

      return hexOctets.join("");
    }

    const opCodes = hex(rom);
    const opCodesArray = [];
    let i = 0;
    while (opCodes.length > i) {
      opCodesArray.push(opCodes.slice(i, i + 2));
      i += 2;
    }

    return opCodesArray;
  }

  private hexToDecimal = (hex: string): number => parseInt(hex, 16);

  private decimalToHex = (decimal: number): string => decimal.toString(16); // TODO PANU: padding??

  private readBigEndian = (hex: string, start: number, end: number) => {
    // return hex.slice(start, end).split("").reverse().join("");
    return hex.slice(start, end);
  };

  private execute(opCode: string) {
    const majorOperation = opCode[0];

    const nnn = this.hexToDecimal(this.readBigEndian(opCode, 1, 4));
    const nn = this.hexToDecimal(this.readBigEndian(opCode, 2, 4));
    const vX = this.hexToDecimal(opCode[1]);
    const vY = this.hexToDecimal(opCode[2]);
    const n = this.hexToDecimal(opCode[3]);

    const opCode0 = () => {
      switch (opCode) {
        case "00e0": // disp_clear()
          this.initDisplay();
          this.pc += 2;
          return "CLS";
        case "00ee":
          this.pc = this.stack.pop() as number;
          return "RETURN";
        default:
          console.warn(`${opCode} Not supported`);
          this.pc += 2;
      }
    };

    const opCode1 = () => {
      this.pc = nnn;
      return "JUMP";
    };

    const opCodeB = () => {
      this.pc = nnn + this.registers[0]; // TODO PANU: pitäiskö muistia + rekistereitä myös pitää muodossa decimal?
      return "JUMP";
    };

    const opCode2 = () => {
      this.pc += 2;
      this.stack.push(this.pc);
      this.pc = nnn;
      return "CALL";
    };

    const opCode3 = () => {
      if (this.registers[vX] === nn) {
        this.pc += 2;
      }

      this.pc += 2;
      return "IF";
    };

    const opCode4 = () => {
      if (this.registers[vX] !== nn) {
        this.pc += 2;
      }

      this.pc += 2;
      return "IF";
    };

    const opCode5 = () => {
      if (this.registers[vX] === this.registers[vY]) {
        this.pc += 2;
      }

      this.pc += 2;
      return "IF";
    };

    const opCode9 = () => {
      if (this.registers[vX] !== this.registers[vY]) {
        this.pc += 2;
      }

      this.pc += 2;
      return "IF";
    };

    const opCode6 = () => {
      this.registers[vX] = nn;

      this.pc += 2;
      return "SET";
    };

    const opCode7 = () => {
      const orig = this.registers[vX];
      this.registers[vX] += nn;
      this.registers[vX] = this.registers[vX] % 256;
      this.pc += 2;
      return `ADD ${nn} to ${orig} (vX+nn)`;
    };

    const opCodeA = () => {
      this.i = nnn;

      this.pc += 2;
      return "MEM";
    };

    const opCodeC = () => {
      const rand = Math.floor(Math.random() * 255) & nn;
      this.registers[vX] = rand;

      this.pc += 2;
      return "RAND";
    };

    const opCodeD = () => {
      let pixelUnset = 0;

      for (let y = 0; y < n; y++) {
        const sprite = this.hexToDecimal(this.memory[this.i + y]);
        for (let x = 0; x < 8; x++) {
          const flipPixel = ((sprite >> (7 - x)) & 1) === 1;

          if (flipPixel) {
            const yCoord = (y + this.registers[vY]) % 32;
            const xCoord = (x + this.registers[vX]) % 64;

            // const leftPad = (string: any, pad: any) =>
            //   pad.substring(0, pad.length - string.length) + string;
            // console.log(
            //   `drawing ${
            //     this.display[yCoord][xCoord] ? 0 : 1
            //   } (sprite ${leftPad(
            //     sprite.toString(2),
            //     "00000000"
            //   )}) to (x, y) (${xCoord}, ${yCoord})`
            // );

            if (this.display[yCoord][xCoord] === true) pixelUnset = 1;
            this.display[yCoord][xCoord] = !this.display[yCoord][xCoord];
          }
        }
      }
      this.registers[15] = pixelUnset;

      this.pc += 2;
      return "DRAW";
    };

    const opCodeF = () => {
      const minorOpCode = opCode.slice(2, 4);
      switch (minorOpCode) {
        case "1e":
          this.i += this.registers[vX];
          if (this.i > 65536) {
            this.i = this.i % 65536;
            this.registers[15] = 1;
          } else {
            this.registers[15] = 0;
          }
          this.pc += 2;
          return "MEM";

        case "0a":
          let keyCode = null;
          const keyPress = this.keyboard.find((x, idx) => {
            if (x === true) {
              keyCode = idx;
              return true;
            }
            return false;
          });
          if (!keyPress) {
            return "KEYPRESS_WAIT";
          } else {
            this.registers[vX] = keyCode;
            this.pc += 2;
            return "KEYPRESS_GOT";
          }

        case "15":
          this.delayTimer = this.registers[vX];
          this.pc += 2;
          return "TIMER_SET";

        case "07":
          this.registers[vX] = this.delayTimer;
          this.pc += 2;
          return "TIMER_READ";

        case "33":
          const val = new Number(this.registers[vX])
            .toString()
            .padStart(3, "0");
          this.memory[this.i + 0] = this.decimalToHex(parseInt(val[0]));
          this.memory[this.i + 1] = this.decimalToHex(parseInt(val[1]));
          this.memory[this.i + 2] = this.decimalToHex(parseInt(val[2]));

          this.pc += 2;
          return "MEM_STORE";

        case "18":
          this.soundTimer = this.registers[vX];
          this.pc += 2;
          return "SOUND_TIMER";

        case "55":
          for (let i = 0; i <= vX; i++) {
            this.memory[this.i + i] = this.decimalToHex(this.registers[i]);
          }
          this.pc += 2;
          return "MEM_STORE";

        case "65":
          // TODO PANU: should memory be numbers instead of hexes? smells like hex should be display-only property??
          for (let i = 0; i <= vX; i++) {
            this.registers[i] = this.hexToDecimal(this.memory[this.i + i]);
          }
          this.pc += 2;
          return "MEM_LOAD";

        case "29":
          // pointer to font sprite. sprites are 5 bytes each and stored in memory starting from 0
          this.i = this.registers[vX] * 5;

          this.pc += 2;
          return `SET_FONT ${this.registers[vX]}`;

        default:
          console.warn(`${opCode} Not supported`);
          this.pc += 2;
          return "NOT SUPPORTED";
      }
    };

    const opCode8 = () => {
      switch (opCode[3]) {
        case "0":
          this.registers[vX] = this.registers[vY];
          this.pc += 2;
          return "ASSIGN";
        case "1":
          this.registers[vX] = this.registers[vX] | this.registers[vY];
          this.pc += 2;
          return "BITWISE OR";
        case "2":
          this.registers[vX] = this.registers[vX] & this.registers[vY];
          this.pc += 2;
          return "BITWISE AND";
        case "3":
          this.registers[vX] = this.registers[vX] ^ this.registers[vY];
          this.pc += 2;
          return "BITWISE XOR";
        case "4":
          const orig1 = this.registers[vX];
          const orig2 = this.registers[vY];
          this.registers[vX] += this.registers[vY];
          if (this.registers[vX] > 256) {
            this.registers[vX] = this.registers[vX] % 256;
            this.registers[15] = 1;
          } else {
            this.registers[15] = 0;
          }

          this.pc += 2;
          return `ADD ${orig2} to ${orig1} (vX + vY)`;
        case "5":
          this.registers[vX] -= this.registers[vY];

          if (this.registers[vX] < 0) {
            this.registers[vX] += 256;
            this.registers[15] = 0;
          } else {
            this.registers[15] = 1;
          }

          this.pc += 2;
          return "SUB";
        case "6":
          const leastSignificantBit = (this.registers[vX] >> 7) & 1;
          this.registers[0x0f] = leastSignificantBit !== 1;
          this.registers[vX] = Math.floor(this.registers[vX] / 2);
          this.pc += 2;
          return "BITWISE DIVIDE";
        case "7":
          this.registers[vY] -= this.registers[vX];

          if (this.registers[vY] < 0) {
            this.registers[vY] += 256;
            this.registers[15] = 0;
          } else {
            this.registers[15] = 1;
          }

          this.pc += 2;
          return "SUB";
        case "e":
          const mostSignificantBit = (this.registers[vX] >> 0) & 1;
          this.registers[0x0f] = mostSignificantBit === 1;
          this.registers[vX] = this.registers[vX] * 2;
          this.registers[vX] = this.registers[vX] % 256;
          this.pc += 2;
          return "BITWISE MULTIPLE";
        default:
          console.warn(`${opCode} Not supported`);
          this.pc += 2;
        // throw new Error(`${opCode} Not supported`);
      }
    };

    const opCodeE = () => {
      if (opCode.slice(2, 4) === "9e") {
        // console.log("reading " + this.registers[vX]);
        if (this.keyboard[this.registers[vX]] === true) {
          this.pc += 2;
        }
      } else if (opCode.slice(2, 4) === "a1") {
        // console.log("reading " + this.registers[vX]);
        if (this.keyboard[this.registers[vX]] === false) {
          this.pc += 2;
        }
      }
      this.pc += 2;
      return "KEYB";
    };

    // TODO PANU: only reading arrayBuffer might give more performance?
    switch (majorOperation) {
      case "0":
        return opCode0();
      case "1":
        return opCode1();
      case "2":
        return opCode2();
      case "3":
        return opCode3();
      case "4":
        return opCode4();
      case "5":
        return opCode5();
      case "6":
        return opCode6();
      case "7":
        return opCode7();
      case "8":
        return opCode8();
      case "9":
        return opCode9();
      case "a":
        return opCodeA();
      case "b":
        return opCodeB();
      case "c":
        return opCodeC();
      case "d":
        return opCodeD();
      case "e":
        return opCodeE();
      case "f":
        return opCodeF();
      default:
        debugger;
        throw new Error(`${opCode} Not supported`);
    }
  }
}
