import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { chip8 } from "./chip-8/chip-8";

const loadRom = (rom: string) =>
  fetch(rom).then((x) => {
    x.arrayBuffer().then((rom) => {
      const runtime = new chip8(rom);

      ReactDOM.render(
        <React.StrictMode>
          <App runtime={runtime} />
        </React.StrictMode>,
        document.getElementById("root")
      );
    });
  });

let rom = window.localStorage.getItem("rom");
if (!rom) {
  rom = "roms/games/Space Invaders [David Winter].ch8";
  window.localStorage.setItem("rom", rom);
}
loadRom(rom);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
