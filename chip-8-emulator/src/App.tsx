import React, { useState, useEffect, useRef } from "react";
import "./App.css";
import { chip8 } from "./chip-8/chip-8";

type Props = {
  runtime: chip8;
};

function useInterval(callback: Function, delay: number) {
  const savedCallback: any = useRef();
  useEffect(() => {
    savedCallback.current = callback;
  });
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    let id = setInterval(tick, delay);
    return () => clearInterval(id);
  }, [delay]);
}

const options = [
  "roms/games/15 Puzzle [Roger Ivie] (alt).ch8",
  "roms/games/15 Puzzle [Roger Ivie].ch8",
  "roms/games/Addition Problems [Paul C. Moews].ch8",
  "roms/games/Airplane.ch8",
  "roms/games/Animal Race [Brian Astle].ch8",
  "roms/games/Astro Dodge [Revival Studios, 2008].ch8",
  "roms/games/Biorhythm [Jef Winsor].ch8",
  "roms/games/Blinky [Hans Christian Egeberg, 1991].ch8",
  "roms/games/Blinky [Hans Christian Egeberg] (alt).ch8",
  "roms/games/Blitz [David Winter].ch8",
  "roms/games/Bowling [Gooitzen van der Wal].ch8",
  "roms/games/Breakout (Brix hack) [David Winter, 1997].ch8",
  "roms/games/Breakout [Carmelo Cortez, 1979].ch8",
  "roms/games/Brick (Brix hack, 1990).ch8",
  "roms/games/Brix [Andreas Gustafsson, 1990].ch8",
  "roms/games/Cave.ch8",
  "roms/games/Coin Flipping [Carmelo Cortez, 1978].ch8",
  "roms/games/Connect 4 [David Winter].ch8",
  "roms/games/Craps [Camerlo Cortez, 1978].ch8",
  "roms/games/Deflection [John Fort].ch8",
  "roms/games/Figures.ch8",
  "roms/games/Filter.ch8",
  "roms/games/Guess [David Winter] (alt).ch8",
  "roms/games/Guess [David Winter].ch8",
  "roms/games/Hi-Lo [Jef Winsor, 1978].ch8",
  "roms/games/Hidden [David Winter, 1996].ch8",
  "roms/games/Kaleidoscope [Joseph Weisbecker, 1978].ch8",
  "roms/games/Landing.ch8",
  "roms/games/Lunar Lander (Udo Pernisz, 1979).ch8",
  "roms/games/Mastermind FourRow (Robert Lindley, 1978).ch8",
  "roms/games/Merlin [David Winter].ch8",
  "roms/games/Missile [David Winter].ch8",
  "roms/games/Most Dangerous Game [Peter Maruhnic].ch8",
  "roms/games/Nim [Carmelo Cortez, 1978].ch8",
  "roms/games/Paddles.ch8",
  "roms/games/Pong (1 player).ch8",
  "roms/games/Pong (alt).ch8",
  "roms/games/Pong 2 (Pong hack) [David Winter, 1997].ch8",
  "roms/games/Pong [Paul Vervalin, 1990].ch8",
  "roms/games/Programmable Spacefighters [Jef Winsor].ch8",
  "roms/games/Puzzle.ch8",
  "roms/games/Reversi [Philip Baltzer].ch8",
  "roms/games/Rocket Launch [Jonas Lindstedt].ch8",
  "roms/games/Rocket Launcher.ch8",
  "roms/games/Rocket [Joseph Weisbecker, 1978].ch8",
  "roms/games/Rush Hour [Hap, 2006] (alt).ch8",
  "roms/games/Rush Hour [Hap, 2006].ch8",
  "roms/games/Russian Roulette [Carmelo Cortez, 1978].ch8",
  "roms/games/Sequence Shoot [Joyce Weisbecker].ch8",
  "roms/games/Shooting Stars [Philip Baltzer, 1978].ch8",
  "roms/games/Slide [Joyce Weisbecker].ch8",
  "roms/games/Soccer.ch8",
  "roms/games/Space Flight.ch8",
  "roms/games/Space Intercept [Joseph Weisbecker, 1978].ch8",
  "roms/games/Space Invaders [David Winter] (alt).ch8",
  "roms/games/Space Invaders [David Winter].ch8",
  "roms/games/Spooky Spot [Joseph Weisbecker, 1978].ch8",
  "roms/games/Squash [David Winter].ch8",
  "roms/games/Submarine [Carmelo Cortez, 1978].ch8",
  "roms/games/Sum Fun [Joyce Weisbecker].ch8",
  "roms/games/Syzygy [Roy Trevino, 1990].ch8",
  "roms/games/Tank.ch8",
  "roms/games/Tapeworm [JDR, 1999].ch8",
  "roms/games/Tetris [Fran Dachille, 1991].ch8",
  "roms/games/Tic-Tac-Toe [David Winter].ch8",
  "roms/games/Timebomb.ch8",
  "roms/games/Tron.ch8",
  "roms/games/UFO [Lutz V, 1992].ch8",
  "roms/games/Vers [JMN, 1991].ch8",
  "roms/games/Vertical Brix [Paul Robson, 1996].ch8",
  "roms/games/Wall [David Winter].ch8",
  "roms/games/Wipe Off [Joseph Weisbecker].ch8",
  "roms/games/Worm V4 [RB-Revival Studios, 2007].ch8",
  "roms/games/X-Mirror.ch8",
  "roms/games/ZeroPong [zeroZshadow, 2007].ch8",
  "roms/demos/Maze (alt) [David Winter, 199x].ch8",
  "roms/demos/Maze [David Winter, 199x].ch8",
  "roms/demos/Particle Demo [zeroZshadow, 2008].ch8",
  "roms/demos/Sierpinski [Sergey Naydenov, 2010].ch8",
  "roms/demos/Sirpinski [Sergey Naydenov, 2010].ch8",
  "roms/demos/Stars [Sergey Naydenov, 2010].ch8",
  "roms/demos/Trip8 Demo (2008) [Revival Studios].ch8",
  "roms/demos/Zero Demo [zeroZshadow, 2007].ch8",
  "roms/hires/Astro Dodge Hires [Revival Studios, 2008].ch8",
  "roms/hires/Hires Maze [David Winter, 199x].ch8",
  "roms/hires/Hires Particle Demo [zeroZshadow, 2008].ch8",
  "roms/hires/Hires Sierpinski [Sergey Naydenov, 2010].ch8",
  "roms/hires/Hires Stars [Sergey Naydenov, 2010].ch8",
  "roms/hires/Hires Test [Tom Swan, 1979].ch8",
  "roms/hires/Hires Worm V4 [RB-Revival Studios, 2007].ch8",
  "roms/hires/Trip8 Hires Demo (2008) [Revival Studios].ch8",
  "roms/programs/BMP Viewer - Hello (C8 example) [Hap, 2005].ch8",
  "roms/programs/Chip8 emulator Logo [Garstyciuks].ch8",
  "roms/programs/Chip8 Picture.ch8",
  "roms/programs/Clock Program [Bill Fisher, 1981].ch8",
  "roms/programs/Delay Timer Test [Matthew Mikolay, 2010].ch8",
  "roms/programs/Division Test [Sergey Naydenov, 2010].ch8",
  "roms/programs/Fishie [Hap, 2005].ch8",
  "roms/programs/Framed MK1 [GV Samways, 1980].ch8",
  "roms/programs/Framed MK2 [GV Samways, 1980].ch8",
  "roms/programs/IBM Logo.ch8",
  "roms/programs/Jumping X and O [Harry Kleinberg, 1977].ch8",
  "roms/programs/Keypad Test [Hap, 2006].ch8",
  "roms/programs/Life [GV Samways, 1980].ch8",
  "roms/programs/Minimal game [Revival Studios, 2007].ch8",
  "roms/programs/Random Number Test [Matthew Mikolay, 2010].ch8",
  "roms/programs/SQRT Test [Sergey Naydenov, 2010].ch8",
  "roms/debug/BC_test.ch8",
];

const Dropdown = ({
  options,
  onChange,
}: {
  options: string[];
  onChange: (option: string) => void;
}) => {
  var rom = window.localStorage.getItem("rom") || "";
  const selectedOption = options.find((x) => x === decodeURIComponent(rom));
  return (
    <select
      value={selectedOption}
      onChange={(e) => {
        if (selectedOption !== e.target.value) {
          onChange(e.target.value);
        }
      }}
    >
      {options.map((o) => (
        <option value={o} key={o}>
          {o}
        </option>
      ))}
    </select>
  );
};

const Chip8Wrapper = ({ runtime }: Props) => {
  const [soundEnabled, setSoundEnabled] = useState(
    window.localStorage.getItem("soundEnabled") !== "false"
  );

  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [log, setLog] = useState([""]);
  const [run, setRun] = useState(0); // 0 = stopped, 1 = run, 2 = fast run
  useEffect(() => {
    window.localStorage.setItem("soundEnabled", soundEnabled.toString());
  }, [soundEnabled]);

  // CHIP-8 has 500Hz CPU so it executes 1 opcode per 2ms

  // this function calls emulation 1ms intervals to see if update is needed
  // note that if updates etc take more time, this function is called less often than in 1ms intervals
  // if that happens, multiple opcodes are run before applying the changes to the screen to hit the 500Hz CPU
  useInterval(
    () => {
      try {
        const execution = runtime.step(run === 2);
        setLog([...log].concat(execution));
      } catch (ex) {
        setRun(0);
        throw ex;
      }
    },
    run ? 1 : 99999999999
  );

  var draw = (
    x: number,
    y: number,
    fill: boolean,
    wSize?: number,
    hSize?: number
  ) => {
    let ctx = canvasRef.current?.getContext("2d");
    if (ctx) {
      if (!wSize || !hSize) {
        wSize = 10;
        hSize = 10;
      }
      if (fill) {
        ctx.fillRect(x, y, wSize, hSize);
      }
      if (!fill) {
        ctx.clearRect(x, y, wSize, hSize);
      }
    }
  };
  return (
    <>
      <div style={{ float: "left" }}>
        <button
          onClick={() => {
            setLog([...log].concat(runtime.stepOnce()));
          }}
        >
          Step
        </button>
        <button onClick={() => setRun(1)}>Run</button>
        <button onClick={() => setRun(2)}>Fast run</button>
        <button
          onClick={() => {
            setRun(0);
            runtime.time = 0;
          }}
        >
          Stop
        </button>
        <button onClick={() => window.location.reload()}>Reset</button>
        {soundEnabled ? (
          <button onClick={() => setSoundEnabled(false)}>Disable sound</button>
        ) : (
          <button onClick={() => setSoundEnabled(true)}>Enable sound</button>
        )}
      </div>
      {run !== 2 ? (
        <div style={{ float: "left" }}>
          <table>
            <thead>
              <tr>
                <th></th>
                <th>Dec</th>
                <th>Hex</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Program counter</td>
                <td>{runtime.pc}</td>
                <td></td>
              </tr>
              <tr>
                <td>I</td>
                <td>{runtime.i}</td>
                <td></td>
              </tr>
              <tr>
                <td>Timer</td>
                <td>{runtime.delayTimer}</td>
                <td></td>
              </tr>
              <tr>
                <td>Next opcode</td>
                <td></td>
                <td>
                  {runtime.memory[runtime.pc] + runtime.memory[runtime.pc + 1]}
                </td>
              </tr>
              {runtime.registers.map((register, i) => {
                return (
                  <tr key={i}>
                    <td>Register {i}</td>
                    <td>{register}</td>
                    <td>
                      {new Number(register).toString(16).padStart(2, "0")}
                    </td>
                  </tr>
                );
              })}

              {/* {runtime.memory.map((register, i) => {
                return (
                  <tr key={i}>
                    <td>Memory {i}</td>
                    <td>{register}</td>
                    <td>
                      {new Number(register).toString(16).padStart(2, "0")}
                    </td>
                  </tr>
                );
              })} */}

              {/* <tr>
              <td>Memory</td>
              <td
                  colSpan={2}
                  style={{
                    wordBreak: "break-all",
                    width: "16em",
                    fontSize: "0.5em",
                  }}
                >
                  {runtime.memory}
                </td>
              </tr> */}
            </tbody>
          </table>
        </div>
      ) : (
        <></>
      )}
      <div style={{ float: "left" }}>
        <h3>Display</h3>
        {runtime.display.map((yVal, yIdx) =>
          yVal.map((xVal, xIdx) =>
            draw(xIdx * 10, yIdx * 10, xVal ? true : false)
          )
        )}
        <canvas
          ref={canvasRef}
          height={320}
          width={640}
          style={{
            border: "1px solid black",
          }}
        >
          Browser is not supporting canvas, using fallback display
          <table>
            <tbody>
              {runtime.display.map((yVal) => (
                <tr>
                  {yVal.map((xVal) => (
                    <td
                      style={{
                        backgroundColor: xVal ? "black" : "white",
                        width: "5px",
                        height: "5px",
                      }}
                    ></td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </canvas>
      </div>

      <div style={{ float: "left" }}>
        <h3>Log</h3>
        {[...log]
          .reverse()
          .slice(0, 100)
          .map((m, idx) => {
            return <div key={idx}>{m}</div>;
          })}
      </div>
    </>
  );
};

const App = ({ runtime }: Props) => {
  return (
    <>
      <div>
        <h2>Chip-8 emulator</h2>
        <p>
          This app emulates{" "}
          <a href="https://en.wikipedia.org/wiki/CHIP-8">CHIP-8 device</a>. This
          is WIP and might contain bugs. Source code can be found in{" "}
          <a href="https://bitbucket.org/panula/random/src/master/chip-8-emulator/">
            https://bitbucket.org/panula/random/src/master/chip-8-emulator/
          </a>
        </p>
        <ol>
          <li>Select ROM</li>
          <li>Click "Fast run" to run ROM</li>
          <li>Click "Run" to see opcodes being executed</li>
          <li>Click "Step" to step one opcode</li>
          <li>Keyboard has 16 keys</li>
          <li>
            Keys are mapped to 1, 2, 3, 4, q, w, e, r, a, s, d, f, z, x, c, v
          </li>
          <li>
            Most games work with <b>q, w, e</b> keys
          </li>
          <li>
            Space Invaders (default ROM) starts the game with w, moves with q,e
            and shoots with w
          </li>
          <li>Tetris rotates with q, moves with w,e and speedups with r</li>
        </ol>
      </div>
      <div>
        ROM&nbsp;
        <Dropdown
          options={options}
          onChange={(rom) => {
            window.localStorage.setItem("rom", rom);
            window.location.reload();
          }}
        ></Dropdown>
      </div>
      <Chip8Wrapper runtime={runtime}></Chip8Wrapper>
    </>
  );
};

export default App;
