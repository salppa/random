Similar brute-force sudoku solver. Written in JS and ported to .NET.

Run instructions:
* JS: open sudoku-solver-js/sudokusolver.html
* dotnet core: go to program dir && dotnet run -c Release

Some approximate performnace benchmarks using hard-coded example sudoku. .NET examples use release flags.
* 800ms .NET core
* 1500ms JS, Chrome
* 1800ms JS, FireFox
* 1900ms .NET
